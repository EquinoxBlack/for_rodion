class LeadsPage < AbstractPage
  include PageObject
  include RelatedToPicker


  page_url "https://app.futuresimple.com/leads"

  button(:add_new_lead, :css => "#leads-new")
  list_items(:leads, :css => ".object-list-item")
  links(:leads_titles, :class => "lead-name")


  def open_lead(name)
    leads_titles_elements.find{|item| item.text.include? name}.click
    sleep 3
  end
end

