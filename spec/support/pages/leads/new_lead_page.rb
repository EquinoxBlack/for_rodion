class NewLeadPage < AbstractPage
  include PageObject

  page_url "https://app.futuresimple.com/leads/new"

  text_field(:first_name, :id => "lead-first-name")
  text_field(:last_name, :id => "lead-last-name")
  text_field(:company_name, :id => "lead-company-name")
  select_list(:lead_status, :name => 'status_id')
  button(:save_lead, :css => ".save")

  def create_lead (name, surname)
    first_name_element.when_visible(10).value = name
    self.last_name = surname
    self.company_name = "Home"
    self.lead_status = "Working"
    save_lead
    sleep 3
  end

end
