class LeadPage < AbstractPage
  include PageObject

  text_area(:add_note, :name => 'note')
  button(:save_new_note, :class => "btn btn-inverse hide")
  unordered_list(:tasks_list, :css => ".item.note-activity")
  span(:lead_title, :class => "detail-title")
  list_items(:notes, :class => "note-activity")
  div(:success_message, :css => '#feedback-nods div')

  def create_note(text)
    self.add_note = text
    save_new_note
  end
  
end
