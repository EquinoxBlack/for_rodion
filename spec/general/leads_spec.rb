require 'spec_helper'



describe "Leads feature" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end


  context "For new lead" do
    before :all do
      @name = Faker::Lorem.word
      @surname = Faker::Lorem.word
      on(LeadsPage).add_new_lead_element.when_visible(10).click
      on(NewLeadPage).create_lead(@name, @surname)
    end

    context "When created" do
      before :all do
        visit(LeadsPage)
        sleep(2)
      end
      it "appears on the leads page" do
        expect(@current_page.leads_titles_elements.map(&:text)).to include(@name + " " +@surname )
      end


      describe "Lead details page" do
        before :all do
          on(LeadsPage).open_lead(@name)
        end
        context 'when opened' do
          it "has appropriate title" do
            expect(on(LeadPage).lead_title).to include @name
          end
          describe "Note creation:" do
            context "when text submitted" do
              before :all do
                on(LeadPage).create_note(Faker::Lorem.sentence)
              end
              it 'shows success message' do
                expect(@current_page.success_message_element.when_present).to be_visible
              end
              it 'appears in notes list' do
                expect(@current_page.notes_elements.count).to eql 1
              end
            end
          end
        end
      end
    end
  end
end